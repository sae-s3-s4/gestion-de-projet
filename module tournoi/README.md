# EPIC: MODULE DE TOURNOI

## US-13 INSCRIPTION AUX TOURNOIS

### Description

En tant qu'**utilisateur**

Je souhaite **m'inscrire à des tournois programmés**

Afin de **participer à la compétition et me mesurer à d'autres joueurs.**

### Critères d'acceptation

- **Étant donné que** je suis intéressé par la participation à un tournoi,
- **Lorsque je** navigue dans la section des tournois,
- **Alors** je peux m'inscrire à un tournoi de mon choix et recevoir une confirmation de mon inscription.

---

## US-14 CRÉATION ET GESTION DE TOURNOIS

### Description

En tant qu'**utilisateur**

Je veux **créer et gérer des tournois**

Afin de **organiser des compétitions et inviter d'autres joueurs à y participer.**

### Critères d'acceptation

- **Étant donné que** je souhaite organiser un tournoi,
- **Lorsque je** crée un nouveau tournoi dans l'application,
- **Alors** je peux définir ses paramètres (nom, date, nombre de participants, etc.) et gérer les inscriptions.

---

## US-15 SUIVI DE L'AVANCEMENT DANS UN TOURNOI

### Description

En tant qu'**utilisateur**

Je souhaite **suivre mon avancement dans un tournoi actif**

Afin de **savoir où je me situe dans la compétition et planifier mes prochaines actions.**

### Critères d'acceptation

- **Étant donné que** je suis inscrit à un tournoi en cours,
- **Lorsque je** consulte le tableau de bord du tournoi,
- **Alors** je peux voir mon avancement actuel, les résultats des matches précédents et les informations sur les prochaines étapes du tournoi.
