# EPIC: MATCHMAKING

## US-3 CRÉATION D'UNE PARTIE PRIVÉ

### Description

En tant qu'**utilisateur**

Je souhaite **créer une nouvelle partie de coinche**

Afin de **jouer avec des amis.**

### Critères d'acceptation

- **Étant donné que** je suis un utilisateur connecté sur la plateforme,
- **Lorsque je** sélectionne l'option de créer une nouvelle partie,
- **Alors** une nouvelle partie est créée et je peux inviter des amis à rejoindre en partageant le code de la partie.

---

## US-4 REJOINDRE UNE PARTIE AVEC UN CODE

### Description

En tant qu'**utilisateur**

Je veux **rejoindre une partie existante à l'aide d'un code de partie**

Afin de **participer à une coinche avec d'autres joueurs.**

### Critères d'acceptation

- **Étant donné que** je suis un utilisateur ayant reçu un code de partie,
- **Lorsque je** saisis ce code dans la section prévue à cet effet,
- **Alors** je suis ajouté à la partie correspondante et peux commencer à jouer si la partie est prête à démarrer.

---

## US-5 REJOINDRE UNE PARTIE VIA MATCHMAKING

### Description

En tant qu'**utilisateur**

Je souhaite **rejoindre une partie de coinche via un système de matchmaking automatique**

Afin de **jouer rapidement sans avoir besoin d'un code de partie.**

### Critères d'acceptation

- **Étant donné que** je suis un utilisateur cherchant à jouer une partie de coinche,
- **Lorsque je** sélectionne l'option de rejoindre une partie via matchmaking,
- **Alors** je suis automatiquement mis en file d'attente et j'entre dans la première partie disponible.
