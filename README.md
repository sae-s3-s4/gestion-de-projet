# Gestion de Projet COINCHOTE

## Aperçu

Le projet COINCHOTE vise à développer une application web solide pour jouer au jeu de cartes classique de la coinche. Ce document décrit notre approche de gestion de projet, en appliquant une méthode agile de développement.

## Méthodologie Agile

Nous adoptons une méthodologie de gestion de projet agile avec une approche Kanban.

## Kanban

Notre tableau Kanban sera l'outil central de suivi des progrès. Il reflétera les tâches actuelles, indiquant ce qui doit être fait, ce qui est en cours et ce qui a été complété.

## User Stories et Issues

Toutes les tâches de développement seront basées sur des User Stories, qui sont détaillées de manière exhaustive dans ce dépôt. Chaque User Story sera traduite en plusieurs tâches au sein des dépôts respectifs et représentées sous formes d'issues.

## Jalons (Milestones)

Les jalons seront utilisés pour suivre la progression de ces issues à travers les dépôts. Ils correspondront aux User Stories et aideront à suivre l'achèvement des fonctionnalités.

## EPICs

Le projet est divisé en EPICs majeurs suivants, représentant les domaines fonctionnels principaux :

![Schéma Modules Coinchote](./ModulesJeu.png)

- [**GESTION COMPTE UTILISATEUR**](./gestion%20compte%20utilisateur/README.md) : Gestion de la création, de l'authentification et de la gestion des comptes utilisateurs.
- [**MATCHMAKING**](./matchmaking/README.MD) : Permettre aux utilisateurs de rejoindre et de créer des lobbies de jeu.
- [**MODULE JEU**](./module%20jeu/README.md) : La mécanique de jeu et les interfaces utilisateur.
- [**CLASSEMENT**](./classement/README.md) : Gestion des classements et des évaluations des joueurs.
- [**MODULE TOURNOI**](./module%20tournoi/README.md) : Mise en place, gestion et progression des tournois.

## Conclusion

Ce README sert de guide pour le processus de développement et la manière dont nous gérons et suivons le travail.
