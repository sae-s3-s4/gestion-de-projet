# EPIC: Gestion Compte Utilisateur

## Aperçu

Cet EPIC se concentre sur la mise en place de la gestion des comptes utilisateurs dans l'application COINCHOTE. Il inclut tout le processus d'inscription et de connexion.

## User Stories

Les User Stories suivantes sont incluses dans cet EPIC :

### US 1: [Inscription avec e-mail et mot de passe](./US-1%20INSCRIPTION.md)

Un nouvel utilisateur peut créer un compte en utilisant son adresse e-mail et un mot de passe. L'utilisateur recevra un e-mail de confirmation pour activer son compte.

### US 2: [Processus de connexion](./US-2%20CONNEXION.md)

L'utilisateur enregistré pourra se connecter à l'application en utilisant son adresse e-mail et son mot de passe.

