# US-1 INSCRIPTION

## Description

En tant qu'**utilisateur**

je souhaite **m'inscrire avec un email, pseudo et un mot de passe**

afin de **créer mon compte.**

## Critères d'acceptations

- **Étant donné que** je suis un nouvel utilisateur et que je souhaite m'inscrire,
- **Lorsque je** soumets une adresse e-mail valide, un pseudo et un mot de passe conforme aux exigences de sécurité,
- **Alors** mon compte est créé.

---

- **Étant donné que** je suis un nouvel utilisateur en train de m'inscrire,
- **Lorsque je** saisis une adresse e-mail déjà utilisée,
- **Alors** je suis informé que l'adresse e-mail est déjà enregistrée et je suis invité à me connecter.

---

- **Étant donné que** je suis un nouvel utilisateur en train de m'inscrire,
- **Lorsque je** ne remplis pas les critères de complexité du mot de passe,
- **Alors** je suis informé des critères nécessaires et invité à saisir un nouveau mot de passe conforme.
