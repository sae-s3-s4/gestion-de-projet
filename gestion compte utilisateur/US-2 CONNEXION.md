# US-3 CONNEXION

## Description

En tant qu'**utilisateur enregistré**

je souhaite **me connecter à mon compte en utilisant mon email et mon mot de passe**

afin de **accéder aux fonctionnalités de l'application COINCHOTE.**

## Critères d'acceptations

- **Étant donné que** je suis un utilisateur enregistré souhaitant me connecter,
- **Lorsque je** soumets mon adresse e-mail et mon mot de passe corrects,
- **Alors** je suis authentifié et j'accède à mon compte utilisateur.

---

- **Étant donné que** je saisis un mot de passe incorrect lors de la tentative de connexion,
- **Lorsque je** soumets le formulaire de connexion,
- **Alors** je reçois un message d'erreur indiquant que les informations d'identification sont incorrectes et la possibilité de réessayer.
