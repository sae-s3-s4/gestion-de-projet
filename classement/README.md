# EPIC: MODULE DE CLASSEMENT

## US-11 VISUALISATION DU CLASSEMENT PERSONNEL

### Description

En tant qu'**utilisateur**

Je souhaite **voir mon classement par rapport aux autres joueurs**

Afin de **mesurer mes performances et ma progression.**

### Critères d'acceptation

- **Étant donné que** je suis connecté à mon compte utilisateur,
- **Lorsque je** consulte la section classement,
- **Alors** je peux voir ma position, mon score et mon évolution dans le classement par rapport aux autres joueurs.

---

## US-12 VISUALISATION DU CLASSEMENT GÉNÉRAL

### Description

En tant qu'**utilisateur**

Je veux **voir les scores des meilleurs joueurs dans un classement général**

Afin de **évaluer la compétitivité du jeu et identifier les hauts niveaux de performance.**

### Critères d'acceptation

- **Étant donné que** je souhaite connaître les performances des meilleurs joueurs,
- **Lorsque je** navigue sur le tableau du classement général,
- **Alors** les scores et les pseudos des meilleurs joueurs sont affichés, me permettant de voir qui domine dans le jeu.
