
# EPIC: MODULE DE JEU

## US-6 AFFICHAGE DES CARTES

### Description

En tant que **joueur**

Je veux **voir mes cartes et celles jouées sur le plateau de jeu**

Afin de **planifier ma stratégie en cours de jeu.**

### Critères d'acceptation

- **Étant donné que** je suis un joueur dans une partie en cours,
- **Lorsque je** suis connecté au jeu,
- **Alors** mes cartes sont visibles uniquement pour moi et les cartes jouées sont visibles sur le plateau pour tous les joueurs. Le dernier pli est également accessible pour tous les joueurs.

---

## US-7 SÉLECTION ET JEU DE LA CARTE

### Description

En tant que **joueur**

Je veux **pouvoir sélectionner la carte à jouer et soumettre mon choix**

Afin de **participer activement à la partie de coinche.**

### Critères d'acceptation

- **Étant donné que** c'est mon tour de jouer,
- **Lorsque je** clique sur une carte de ma main,
- **Alors** la carte est sélectionnée et, après confirmation, jouée sur le plateau.

---

## US-8 ENCHÈRES ET PASSAGE DE TOUR

### Description

En tant que **joueur**

Je souhaite **pouvoir annoncer une enchère ou passer mon tour**

Afin de **déterminer le contrat de la partie de coinche ou laisser la main.**

### Critères d'acceptation

- **Étant donné que** c'est la phase d'enchères,
- **Lorsque je** décide de faire une enchère ou de passer,
- **Alors** mon choix est enregistré et le tour passe au joueur suivant conformément aux règles de la coinche.

---

## US-9 VISUALISATION DU SCORE

### Description

En tant que **joueur**

Je veux **voir le score de la partie en temps réel**

Afin de **connaître l'état actuel du jeu et ajuster ma stratégie.**

### Critères d'acceptation

- **Étant donné que** la partie est en cours,
- **Lorsque je** regarde l'interface du jeu,
- **Alors** le score actuel de la partie est affiché en permanence sur l'écran.

---

## US-10 COMMUNICATION ENTRE JOUEURS

### Description

En tant que **joueur**

Je veux **avoir la possibilité d'envoyer des messages aux autres joueurs durant la partie**

Afin de **communiquer pour une expérience plus conviviale.**

### Critères d'acceptation

- **Étant donné que** je suis un joueur et je souhaite communiquer avec d'autres participants,
- **Lorsque je** tape un message dans le chat en jeu,
- **Alors** mon message est visible par tous les joueurs de la partie dans un espace dédié à la communication.



